/*let promise = new Promise((resolve, reject) => {
    let a = 1 + 1;
    if(a == 2) {
        resolve('Sucess')
    } else {
        reject('Failed')
    }
})

promise.then((message)=> {
    console.log('This is in the then ' + message)
}).catch((message)=> {
    console.log('This is in the catch ' + message)
})
 
console.log("HELLO"); */

const getFruit = async(name) => {
    const fruit = {
        pineapple: 'pia',
        peach: 'pea',
        strawberry: 'strw'
    }

    return fruit[name]
}

getFruit('peach').then((message) => {
    console.log('The fruit is ' + message)
})

const makeSmoothie = async () => {
    const a = getFruit('pineapple')
    const b = getFruit('strawberry')
    const c = await Promise.all([a,b])

    return c
}

makeSmoothie().then((message) => {
    console.log('Make the smoothie with the flavour of ' + message)
})